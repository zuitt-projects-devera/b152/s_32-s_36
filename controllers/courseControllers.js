const Course = require('../models/Course');
const User = require("../models/User");

module.exports.createCourseController = (req, res) => {
//console.log(req.body);

	//Task model is a constructor.
	//newTask that will be created from our Task model will have additional methods to be used in our app
    let newCourse = new Course({
    	name: req.body.name,
    	description: req.body.description,
    	price: req.body.price
	});


    newCourse.save()
    .then(result => res.send(result))
    .catch(error => res.send(error));
}


module.exports.getAllCourseController = (req, res) => {

	Course.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error));

}


module.exports.getSingleCourseController = (req, res) => {

	Course.findById(req.params.id)
	.then(result => res.send(result))
	.catch(error => res.send(error));

}

module.exports.updateCourseController = (req, res) => {
	

	let courseUpdates = { 
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}

	Course.findByIdAndUpdate(req.params.id, courseUpdates, {new: true})
	.then(updatedCourse => res.send(updatedCourse))
	.catch(error => res.send(err));
}

module.exports.archiveCourseController = (req, res) => {

	let courseUpdates = { 
		isActive: false
	}

	Course.findByIdAndUpdate(req.params.id, courseUpdates, {new: true})
	.then(updatedCourse => res.send(updatedCourse))
	.catch(error => res.send(err));

}

module.exports.activateCourseController = (req, res) => {

	let courseUpdates = { 
		isActive: true
	}

	Course.findByIdAndUpdate(req.params.id, courseUpdates, {new: true})
	.then(updatedCourse => res.send(updatedCourse))
	.catch(error => res.send(err));

}

module.exports.getAllActiveCoursesController = (req, res) => {

	Course.find({isActive: true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

module.exports.getAllArchivedCoursesController = (req, res) => {

	Course.find({isActive: false})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}


module.exports.checkCourseNameExists = (req, res) => {

	Course.find({name: { $regex: req.body.name, $options: 'i' }})
	.then(foundName => {
		//console.log(result)
		//an empty array is not equal to null
		if(foundName.length === 0) {
			return res.send("No course found.");
		}
		else {
			return res.send(foundName);
		}
	})
	.catch(error => res.send(error));
}

module.exports.checkPriceExists = (req, res) => {

	Course.find({price: req.body.price})
	.then(foundPrice => {
		//console.log(result)
		if(foundPrice.length === 0) {
			return res.send("No course found.");
		}
		else {
			return res.send(foundPrice);
		}
	})
	.catch(error => res.send(error));
}


module.exports.getEnrolleesControllers = (req, res) => {
	Course.findById(req.params.id)
		.then(courseEnrollees => {
			return res.send(courseEnrollees.enrollees)
		}).catch(err => err.message)
}