//import Models
const User = require("../models/User");
const Course = require("../models/Course");

//import bcrypt
const bcrypt = require("bcrypt");
//bcrypt is a package which will help us add a layer of security for our user's passwords.

//auth module
const auth = require("../auth");
//console.log(auth);

//createUser
module.exports.registerUser = (req,res) => {


	/*
		bcrypt adds a layer of security to our user's password.

		What bcrypt does is hash our password into a randomized character version of the original string.

		It is able to hide your password within that randomized string.
		
		syntax:
		bcrypt.hashSync(<stringToBeHashed>,<saltRounds>)

		Salt-Rounds are the number of times the characters in the hash are randomized

	*/

	//create a variable to store the hashed/encrypted password. Then this new hashed pw will be what save 
	//in our database.

	User.findOne({email: req.body.email})
	.then(result => {
		//console.log(result)
		if(result !== null && result.email === req.body.email) {
			return res.send("Duplicate User Found");
		}
		else {
			const hashedPW = bcrypt.hashSync(req.body.password, 10);
			//console.log(hashedPW);//IF you want to check if the password was hashed.
			//Create a new user document out of our User model:
			let newUser = new User({
				firstName: req.body.firstName,
				lastName: req.body.lastName,
				email: req.body.email,
				mobileNo: req.body.mobileNo,
				password: hashedPW

			});
			newUser.save()
			.then(user => res.send(user))
			.catch(err => res.send(err));
				}
	})
	.catch(error => res.send(err));
	
}

//getAllUsers
module.exports.getAllUsers = (req,res) => {

	User.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error));

}

//login
module.exports.loginUser = (req, res) => {
	User.findOne({email: req.body.email})
	.then(foundUser => {
		if (foundUser === null) {
			return res.send("User not found.");
		}
		else {
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password);
			/* syntax
			bcrypt.compareSync(<String>, <hashedString>)

			If the string and hashedstring matches, compareSync method from bcrypt will return true.

			If not, it will return false.*/
			//console.log(isPasswordCorrect);
			if(isPasswordCorrect) {
				/* auth.createAcessToken will receive our foundUser as an argument.
				Then, in the createAcessToken() will return a "token" or "key"
				which contains some of our user's details.

				This key is encoded and only our own methods wil be able to decode it. 

				auth.createAccessToken will return a JWT token out of our user details. Then, with 
				res.send*/
			return res.send({acessToken: auth.createAccessToken(foundUser)});
			}
			else {
			return res.send("Incorrect password.");
			}
		}
	})
	.catch(error => res.send(error));

}

//getUserDetails
module.exports.getUserDetails = (req, res) => {
	/* Find our logged in user's document from our db and send it to the client. by its id*/
	//console.log(req.user);
	 User.findById(req.user.id)
	 .then(result => res.send(result))
	 .catch(error => res.send(error));
	//res.send("testing for verify");
}

//checkEmailExists
module.exports.checkEmailExists = (req, res, next) => {

	User.findOne({email: req.body.email})
	.then(foundEmail => {
		//console.log(result)
		if(foundEmail !== null && foundEmail.email === req.body.email) {
			return res.send("Email is already registered.");
		}
		else {
			next();
		}
	})
	.catch(error => res.send(error));
}

module.exports.updateUserDetails = (req, res) => {

	let updates = {

		firstName: req.body.firstName,
		lastName: req.body.lastName,
		mobileNo: req.body.mobileNo

	}

	User.findByIdAndUpdate(req.user.id,updates,{new:true})
	.then(updatedUser => res.send(updatedUser))
}

module.exports.updateUserRoleController = (req, res) => {
	 User.findByIdAndUpdate(req.params.id, {isAdmin:true}, {new: true})
	 .then(result =>  res.send(result))
	 .catch(error => res.send(error));

}

//enroll a user
module.exports.enroll = async (req,res) => {
	
	if(req.user.isAdmin) {
		return res.send("Action Forbidden")
	}

	let isUserUpdated = await User.findById(req.user.id).then(user => {

	
		let newEnrollment = {
			courseId: req.body.courseId
		}

	
		user.enrollments.push(newEnrollment);

	
		return user.save().then(user => true).catch(err => err.message)

	})

	if(isUserUpdated !== true) {
		return res.send({message: isUserUpdated})
	}


	let isCourseUpdated  = await Course.findById(req.body.courseId).then(course => {

		let enrollee = {
			userId: req.user.id
		}

		course.enrollees.push(enrollee);

		return course.save().then(course => true).catch(err => err.message)
	})

	if(isCourseUpdated !== true){
		return res.send({message: isCourseUpdated})
	}

	if(isUserUpdated && isCourseUpdated){
		return res.send({message: "Enrolled Successfully."})
	}

}

module.exports.getEnrollmentsController = (req, res) => {
	User.findById(req.user.id)
		.then(userCourses => {
			return res.send(userCourses.enrollments)
		}).catch(err => err.message)
}

