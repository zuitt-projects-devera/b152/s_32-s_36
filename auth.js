/* We will create our own module which will have methods that will help us authorize 
our users to access or to disallow access to certain parts or features in our app*/

//imports

const jwt = require('jsonwebtoken');


/*JWT is a way to securely pass information from one part of the server to the frontend/client or 
other parts of our application. This will allow us to authorize our users to access or disallow access
to certain parts of our application.

JWT is like a gift wrapping serving able to encode our user details which can only be unwrapped by jwt's
own methods and if the secret provided is intact.

IF the jwt seemed tampered with we will reject the users attempt to access a feature in our app. 
*/

module.exports.createAccessToken = (user) => {

	/*check if we can receive the details of the foundUser in our login:
	console.log(user);

	data onject is created to contain some details of our user*/
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}
	//console.log(data);
	//create our jwt with data payload, our secret and the algorithm to create our JWT token

	return jwt.sign(data, process.env.AUTH_SECRET, {});
}

module.exports.verify = (req,res,next) => {
	//console.log("Hello from verify method in auth.");
	/*middlewares which have access to req, res and next also can send responses to the client.
	
	req.headers.authorization - contains sensitive data and especially our token/jwt
	*/
	//console.log(req.headers.authorization);
	let token = req.headers.authorization;
	/*IF we are not passing token in our request authorization or in our postman authorization, then here in our 
	api, req.headers.authorization will be undefined.

	This if statement will first check IF token variable contains undefined or a proper jwt. If it is undefined, we will
	check token's data type with typeof, then send a message to the client. */

	if (typeof token === "undefined") {
		return res.send({auth: "Failed. No Token."});
	}
	else {
		/*We will check if the token is legit before proceeding to the next middleware/controller.*/
		
		token = token.slice(7, token.length)
		//console.log(token);
		//verify the legtimacy of our token:
		jwt.verify(token, process.env.AUTH_SECRET, function(err,decodedToken) {
			//err will contain the errors from decoding your token. This will contain the reason why we will reject the token.
			if(err) {
				return res.send({
					auth: "Failed",
					message: err.message
				})
			}
			else {
				/*console.log(decodedToken); contains the data from our token we will add a new property to 
				the request object. In fact, anything that we add to the request object, we can still get from our 
				next middleware or controller.*/
				req.user=decodedToken;
				next();
			}
		})
		//next() will let us proceed to the next middleware OR controller
		/*next();*/
	}
}

module.exports.verifyAdmin = (req, res, next) => {
	//verifyAdmin comes after the verify middleware, 
	//Do we have any access to our user's isAdmin details
	
	if(req.user.isAdmin) {
		
		next();
	}
	else {
		return res.send({
			auth: "Failed!",
			message: "Action Forbidden."
		})
	}
}