require('dotenv').config()
const express = require('express'); //import express.js
const mongoose = require('mongoose'); //import mongoose
const app = express(); //
const port = 4000;

//connect api to db:
/*mongoose.connect("mongodb+srv://user1-blue:admin123@cluster0.p9on8.mongodb.net/bookingsystemAPI152?retryWrites=true&w=majority",
	{
		useNewUrlParser: true, 
		useUnifiedTopology:true
	});*/
mongoose.connect(`mongodb+srv://${process.env.DB_USER}:${process.env.DB_PASSWORD}@cluster0.p9on8.mongodb.net/${process.env.DB_DATABASE_NAME}?retryWrites=true&w=majority`,
	{
		useNewUrlParser: true, 
		useUnifiedTopology:true,
		ignoreUndefined: true,
	});

//notifications for successful or failed mongodb connection:
const db = mongoose.connection;


db.on('error', console.error.bind(console, "Connection Error."));
//once the connection is open and successful, we wull output a message in the terminal
db.once('open', ()=>console.log('Connected to MongoDB.'));


//middleware - acts and adds features to our application
app.use(express.json());

const courseRoutes = require('./routes/courseRoutes');
app.use('/courses', courseRoutes);


const userRoutes = require('./routes/userRoutes');
app.use('/users',userRoutes);



app.listen(port, () => console.log(`Server is running at port ${port}`));
