const mongoose = require("mongoose");

let userSchema = new mongoose.Schema({

	firstName: {
		type: String,
		required: [true, "First Name is required."]
	},
	lastName: {
		type: String,
		required: [true, "Last Name is required."]
	},
	email: {
		type: String,
		required: [true, "Email is required."]
	},
	password: {
		type: String,
		required: [true, "Password is required."]
	},
	mobileNo: {
		type: String,
		required: [true, "Mobile Number is required."]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	enrollments: [

		/*
			In mongodb, we can have 2 way embedding for models with many to many relationship. In mongodb, to implement 2 way embedding we add subdocument arrays for both models. Each subdocument array should also have a schema for its subdocuments which are the associative entity created.

		*/
		
		{
			courseId: {
				type: String,
				required: [true, "Course Id is required."]
			},
			dateEnrolled: {
				type: Date,
				default: new Date()
			},
			status: {
				type: String,
				default: "Enrolled"
			}
		}

	]

})

module.exports = mongoose.model("User",userSchema);
