const express = require('express');
const router = express.Router();

const courseContollers = require('../controllers/courseControllers');

const auth = require("../auth");
const {verify, verifyAdmin} = auth;

/*create/addcourse
Since this route should not be access by all users, we have to authorize only some logged in users 
who have the proper authority: 

First, verify if the token the user is using is legit. */
router.post('/', verify, verifyAdmin, courseContollers.createCourseController);

//get all courses
router.get('/', courseContollers.getAllCourseController);

//get single course
router.get('/getSingleCourse/:id', courseContollers.getSingleCourseController);

//update course
router.put('/:id', verify, verifyAdmin, courseContollers.updateCourseController)

//archive course
router.put('/archive/:id', verify, verifyAdmin, courseContollers.archiveCourseController);

//activate course
router.put('/activate/:id', verify, verifyAdmin, courseContollers.activateCourseController);

//retrieve all active courses
router.get('/getActiveCourses', courseContollers.getAllActiveCoursesController);


//retrieve all archived courses
router.get('/getArchivedCourses', verify, verifyAdmin, courseContollers.getAllArchivedCoursesController);

//get all with same name
router.post('/getSameName', courseContollers.checkCourseNameExists);

//get all with same price
router.post('/getSamePrice', courseContollers.checkPriceExists);

router.get('/getEnrollees/:id', verify, verifyAdmin, courseContollers.getEnrolleesControllers);
		


module.exports = router;