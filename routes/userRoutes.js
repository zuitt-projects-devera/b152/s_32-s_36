const express = require("express");
const router = express.Router();

//import user controllers
const userControllers = require("../controllers/userControllers")

//userControllers is a module we imported. In JS, modules are objects.
//exported controllers are methods to their modules.
//console.log(userControllers)

/*We will import auth module so we can use our
verify and verifyAdmin method as middle for
our routes*/
const auth = require('../auth');

/*destructure verify and verifyAdmin from auth.
Auth, is an imported module, so therefore*/
const {verify, verifyAdmin} = auth;


//User Registration:
router.post("/", userControllers.checkEmailExists, userControllers.registerUser);

//retrieve all users
router.get('/', verify, verifyAdmin, userControllers.getAllUsers);

//user login
router.get('/login', userControllers.loginUser);

//get logged in users details
/*verify method from auth will be used as a 
middleware. Middleware are functions we can use
around our app. In the context of expressjs route, middleware are functions we add in the route and can receive 
request, response and next objects. We can have more than 1 middleware before controller.*/
//router.get('<endpoint>', middleware, controller);
//When a has middlewares and controller, we will run through the middleware first before we get to the controller.
/*Note: Routes that have verify as a middleware would require us to pass a token from postman.*/
router.get('/getUserDetails', verify, userControllers.getUserDetails);

//check if the provided email is already in use by one of the users 
router.get('/checkEmailExists', userControllers.checkEmailExists);


//router.put('/updateUserDetails', verify, userControllers.updateUserDetails);

router.put('/updateUserDetails',verify,userControllers.updateUserDetails)

router.put('/updateUserRole/:id', verify, verifyAdmin, userControllers.updateUserRoleController);

router.post('/enrollUser', verify, userControllers.enroll);

router.get('/getEnrollments', verify, userControllers.getEnrollmentsController);
		


module.exports = router;
